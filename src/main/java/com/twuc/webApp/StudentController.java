package com.twuc.webApp;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class StudentController {
    /*
        @RequestMapping("/test")
        public @ResponseBody String greeting() {
            return "test";
        }
    */



    @RequestMapping("/test")
    public String greeting2(String gender, @RequestParam(name = "class") String clazz) {
        return gender + " + " + clazz;
    }
/*
    @PostMapping("/api/students")
    public String create(@RequestBody @Valid Student student) {
        return student.getName();
    }
*/
    @RequestMapping("/api/status")
    public ResponseEntity<String> status(){
        // ResponseEntity entity = new ResponseEntity("test", HttpStatus.ACCEPTED);
        ResponseEntity entity = ResponseEntity.accepted().header("test","123").contentType(MediaType.APPLICATION_JSON).build();
        return entity;
    }

    @PostMapping("/api/students")
    public ResponseEntity<Student> getStudent(@RequestBody Student student){
        return ResponseEntity.status(200).body(student);
    }
//    @RequestMapping("/test")
//    public String greeting2(String gender, @RequestParam(name = "class") String clazz) {
//        return gender+" + "+ clazz;
//    }
}
