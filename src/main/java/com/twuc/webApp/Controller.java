package com.twuc.webApp;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @RequestMapping(value="/api/users/{id}/books")
    public String getBook(@PathVariable String id){
        return String.format("The book for user %s",id);
    }

    @RequestMapping(value="/api/segments/good")
    public String getGood1(){
        return "it chooses /api/segments/good";
    }

    @RequestMapping(value="/api/segments/{segmentName}")
    public String getGood2(@PathVariable String segmentName){
        return "it chooses /api/segments/{segmentName}";
    }

    @RequestMapping(value="/api/?/n")
    public String wildcard(){
        return "test";
    }

    @RequestMapping(value="api/wildcards/*")
    public String anything(){
        return "matched";
    }

    @RequestMapping(value="api/*/wildcards")
    public String anything2(){
        return "matched";
    }

    @RequestMapping(value="/api/wildcard/before/*/after")
    public String anything3(){
        return "matched";
    }

    @RequestMapping(value="/api/*/b/*")
    public String wildcard2(){
        return "matched";
    }

    @RequestMapping(value="/api/**/card3")
    public String wildcard3(){
        return "matched";
    }

    @RequestMapping(value="/api/{name:[A-z]*}/card4")
    public String regex(@PathVariable("name") String name){
        return name;
    }
}
