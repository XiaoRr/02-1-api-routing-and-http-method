package com.twuc.webApp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Student {
    @NotBlank
    private String name;
    //@NotBlank
//    private String gender;
/*
    public Student(@NotBlank String name, String age, String phoneNumber) {
        this.name = name;
        this.age = age;
        this.phoneNumber = phoneNumber;
    }
*/
    //@Email
//    private String email;
    //@Size(min = 3, max = 99)
    private Integer age;

    @JsonProperty(value = "phone_number")
    private String phoneNumber;


    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Student() {
    }

    public Student(String name) {
        this.name = name;
    }

//    public String getGender() {
//        return gender;
//    }
//
//    public void setGender(String gender) {
//        this.gender = gender;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}


/*
@assertTrue
@Size
@Min
@Max
@Email
@NotEmpty
@NetBlank
 */