package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc

public class ControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    public void basicTest() throws Exception {
        mockMvc.perform(get("/api/users/liming/books")).andExpect(content().string("The book for user liming"));
        mockMvc.perform(get("/api/users/lihua/books")).andExpect(content().string("The book for user lihua"));
    }

    @Test
    public void upperCaseTest() throws Exception {
        mockMvc.perform(get("/api/users/liming/BOOKS")).andExpect(status().is(404));
    }

    @Test
    public void segmentsTest() throws Exception {
        mockMvc.perform(get("/api/segments/good")).andExpect(content().string("it chooses /api/segments/good"));
    }

    @Test
    public void wildcardTest() throws Exception {
        mockMvc.perform(get("/api/12/n")).andExpect(status().is(404));
    }

    @Test
    public void wildcardTest2() throws Exception {
        mockMvc.perform(get("/api//n"));
    }

    @Test
    public void anythingTest() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything")).andExpect(content().string("matched"));
    }

    @Test
    public void anythingTest2() throws Exception {
        mockMvc.perform(get("/api/anything/wildcards")).andExpect(content().string("matched"));
    }

    @Test
    public void anythingTest3() throws Exception {
        mockMvc.perform(get("/api/before/after")).andExpect(status().is(404));
    }

    @Test
    public void wildcardTest3() throws Exception {
        mockMvc.perform(get("/api/a/b/c")).andExpect(content().string("matched"));
    }

    @Test
    public void wildcardTest4() throws Exception {
        mockMvc.perform(get("/api/a/b/card3")).andExpect(content().string("matched"));
    }

    @Test
    public void regexTest() throws Exception {
        mockMvc.perform(get("/api/matched/card4")).andExpect(content().string("matched"));
    }

    @Test
    public void noParamTest() throws Exception {
        mockMvc.perform(get("/api/users//books")).andExpect(status().is(404));
    }

}
