package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;


@SpringBootTest
@AutoConfigureMockMvc
public class PathTest {

    @Autowired
    MockMvc mockMvc;

    //2-1
    @Test
    public void pathVariableMatchTest() throws Exception {
        //api/users/2/books
        mockMvc.perform(get("/api/users/2/books")).andExpect(content().string("The book for user 2"));
        mockMvc.perform(get("/api/users/23/books")).andExpect(content().string("The book for user 23"));
    }

    //2-2


}
