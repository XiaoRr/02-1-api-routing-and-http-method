package com.twuc.webApp;
//这是课上作业 09。17


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {


    @Autowired
    MockMvc mockMvc;

    @Test
    public void basicTest() throws Exception {
        mockMvc.perform(get("/test")).andExpect(content().string("test"));
    }

    @Test
    public void test() throws Exception {
        mockMvc.perform(get("/test?gender=female&class=A")).andExpect(content().string("female A"));
    }

    @Test
    public void testRequestBody() throws Exception {
        mockMvc.perform(
                post("/api/students")
                        .content("{\"name\": \"abc\",\"gender\":\"female\",\"email\":\"abc@mail.com\"}")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(status().isOk())
                .andExpect(content().string("abc"))
        ;
    }

    @Test
    public void testBadRequest() throws Exception {
        //missing argument
        mockMvc.perform(
                post("/api/students")
                        .content("{\"name\":\"abc\",\"email\":\"abc@mail.com\"}")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void testBadRequestEmail() throws Exception {
        //wrong email
        mockMvc.perform(
                post("/api/students")
                        .content("{\"name\":\"abc\",\"gender\":\"female\",\"email\":\"1234sadd242.com\"}}")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void testStatus() throws Exception {
        mockMvc.perform(
                post("/api/status")).andExpect(status().isAccepted()).andExpect(content().string("test"));
    }

    @Test
    public void testStudent() throws Exception {
        mockMvc.perform(
                post("/api/students")
                        .content("{\"name\":\"lihua\",\"age\":\"12\",\"phone_number\":\"2801712321\"}")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(status().isOk())
                .andExpect(content().string("{\"name\":\"lihua\",\"age\":\"12\",\"phone_number\":\"2801712321\"}"))
        ;
    }

    @Test
    public void testStudent2() throws Exception {
        mockMvc.perform(
                post("/api/students")
                        .content("{\"name\": \"lihua\",\"phone_number\":\"2801712321\"}")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(status().isOk())
                .andExpect(content().string("{\"name\":\"lihua\",\"phone_number\":\"2801712321\"}"))
        ;
    }
}




